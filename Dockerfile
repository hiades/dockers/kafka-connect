FROM confluentinc/cp-kafka-connect-base:7.5.3 as build

RUN confluent-hub install --no-prompt confluentinc/kafka-connect-jdbc:10.7.4 && \
    confluent-hub install --no-prompt confluentinc/kafka-connect-avro-converter:7.5.3 && \
    confluent-hub install --no-prompt confluentinc/kafka-connect-s3:10.5.7

FROM quay.io/strimzi/kafka:0.39.0-kafka-3.6.1

USER root:root

COPY --from=build /usr/share/confluent-hub-components/confluentinc-kafka-connect-avro-converter /opt/kafka/plugins/kafka-connect-avro-converter
COPY --from=build /usr/share/confluent-hub-components/confluentinc-kafka-connect-s3 /opt/kafka/plugins/kafka-connect-s3
COPY --from=build /usr/share/confluent-hub-components/confluentinc-kafka-connect-jdbc /opt/kafka/plugins/kafka-connect-jdbc

WORKDIR /opt/kafka/plugins

RUN for plugin in kafka-connect-s3 kafka-connect-jdbc; \
    do \
        ln -s /opt/kafka/plugins/kafka-connect-avro-converter/ /opt/kafka/plugins/${plugin}/kafka-connect-avro-converter; \
    done && \
    chown 1001:1001 -R /opt/kafka/plugins

USER 1001